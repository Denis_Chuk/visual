﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Задача_2_Массив
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double[] array;
            array = new double[5];
            array[0] = 1;
            array[1] = 2;   
            array[2] = 3;
            array[3] = 4;
            array[4] = 5;
            double x = array.Sum()/2;
            Console.WriteLine(x);

        }
    }
}
